from django.urls import path
from django.conf import settings
from django.conf.urls.static import static
from . import views

app_name = 'article'

urlpatterns = [
    path('', views.article, name='article'),
    path('news/<slug:slug>/', views.news, name='news'),
    path('tipsandtrick/<slug:slug>/', views.tipsandtrick, name='tipsandtrick'),
    path('search_news/', views.search_news, name="search_news"),
    path('search_tips/', views.search_tips, name="search_tips"),
    path('news_api/', views.get_news, name="get_news"),
    path('tips_api/', views.get_tips, name="get_tips"),
    path('json_news/', views.data_news, name="data_news"),
    path('json_tips/', views.data_tips, name="data_tips"),
]
if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL,
                          document_root=settings.MEDIA_ROOT)