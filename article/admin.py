from django.contrib import admin
from .models import *

# Register your models here.
admin.site.register(News)
admin.site.register(TipsAndTrick)
@admin.register(CommentsNews)
class CommentsNewsAdmin(admin.ModelAdmin):
    list_display = ('user', 'comment', 'post', 'published', 'active')
    list_filter = ('active', 'published')
    search_fields = ('user', 'comment')
    actions = ['reject_comments']

    def reject_comments(self, request, queryset):
        queryset.update(active=False)

@admin.register(CommentsTipsAndTrick)
class CommentsTipsAndTrickAdmin(admin.ModelAdmin):
    list_display = ('user', 'comment', 'post', 'published', 'active')
    list_filter = ('active', 'published')
    search_fields = ('user', 'comment')
    actions = ['reject_comments']

    def reject_comments(self, request, queryset):
        queryset.update(active=False)