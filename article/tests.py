from django.test import TestCase, Client
from django.urls import resolve, reverse
from django.apps import apps
from .views import article, news, tipsandtrick, search_news, search_tips
from .models import News, TipsAndTrick, CommentsNews, CommentsTipsAndTrick
from .apps import ArticleConfig
from account.models import Account
import datetime

tgl = datetime.date(2000,12,19)

# Create your tests here.
class TestApp(TestCase):
    def setUp(self):
        account = Account.objects.create(email="tes@aaa.com", username="tes", password = "aaaa1234")
        news1 = News.objects.create(title= 'a', slug = 'a', date= tgl, content = 'a', image = 'a.png')
        tips1 = TipsAndTrick.objects.create(title= 'b', slug = 'b', date= tgl, content = 'b', image = 'b.png')
        comment_news1 = CommentsNews.objects.create(post = news1, user = account, comment = 'c', published = tgl, active=True)
        comment_tips1 = CommentsTipsAndTrick.objects.create(post = tips1, user = account, comment = 'd', published = tgl, active=True)
        
    def test_article_url_exists(self):
        response = Client().get(reverse('article:article'))
        self.assertEquals(response.status_code, 200)
    
    def test_news_url_exists(self):
        response = Client().get(reverse('article:news', args=['a']))
        self.assertEquals(response.status_code, 200)
    
    def test_tips_and_trick_url_exists(self):
        response = Client().get(reverse('article:tipsandtrick', args=['b']))
        self.assertEquals(response.status_code, 200)
    
    def test_search_news_url_exists(self):
        response = Client().get(reverse('article:search_news'))
        self.assertEquals(response.status_code, 200)
    
    def test_search_tips_url_exists(self):
        response = Client().get(reverse('article:search_tips'))
        self.assertEquals(response.status_code, 200)

    def test_article_using_article_template(self):
        response = Client().get(reverse('article:article'))
        self.assertTemplateUsed(response, 'article.html')
    
    def test_search_news_using_search_news_template(self):
        response = Client().get(reverse('article:search_news'))
        self.assertTemplateUsed(response, 'search_news.html')
    
    def test_search_tips_using_search_tips_template(self):
        response = Client().get(reverse('article:search_tips'))
        self.assertTemplateUsed(response, 'search_tips.html')

    def test_article_using_article_function(self):
        found = resolve('/article/')
        self.assertEqual(found.func, article)
    
    def test_search_news_using_search_news_function(self):
        found = resolve('/article/search_news/')
        self.assertEqual(found.func, search_news)
    
    def test_search_tips_using_search_tips_function(self):
        found = resolve('/article/search_tips/')
        self.assertEqual(found.func, search_tips)

    def test_comment_news_form_success_create_comment(self):
        response = Client().post(reverse('article:news', args=['a']), {
			'comment': 'tes'})
        counting_all_available_comments_news = CommentsNews.objects.all().count()
        self.assertEqual(response.status_code, 200)
        self.assertEqual(counting_all_available_comments_news, 2)
        
    def test_comment_tips_and_trick_form_success_create_comment(self):
        response = Client().post(reverse('article:tipsandtrick', args=['b']), {
			'comment': 'tes'})
        counting_all_available_comments_tips_and_trick = CommentsTipsAndTrick.objects.all().count()
        self.assertEqual(response.status_code, 200)
        self.assertEqual(counting_all_available_comments_tips_and_trick, 2)

    def test_news_model_can_create_news(self):
        counting_all_available_article = News.objects.all().count()
        self.assertEqual(counting_all_available_article, 1)

    def test_tips_and_trick_model_can_create_tips_and_trick(self):
        counting_all_available_article = TipsAndTrick.objects.all().count()
        self.assertEqual(counting_all_available_article, 1)

    def test_html_in_article_contain_any_mandatory_words(self):
        response = Client().get(reverse('article:article'))
        html_kembalian = response.content.decode('utf8')
        self.assertIn('NEWS', html_kembalian)
        self.assertIn('TIPS AND TRICK', html_kembalian)
    
    def test_html_in_search_news_contain_any_mandatory_words(self):
        response = Client().get(reverse('article:search_news'))
        html_kembalian = response.content.decode('utf8')
        self.assertIn('Search News', html_kembalian)

    def test_html_in_search_tips_contain_any_mandatory_words(self):
        response = Client().get(reverse('article:search_tips'))
        html_kembalian = response.content.decode('utf8')
        self.assertIn('Search Tips', html_kembalian)

    def test_news_model_str_method(self):
        a = News(title="MILAN SCUDETTO MUSIM INI AAMIIN")
        self.assertEqual(str(a), "MILAN SCUDETTO MUSIM INI AAMIIN")

    def test_tips_and_trick_model_str_method(self):
        a = TipsAndTrick(title="TIPS AND TRICK SUPAYA JAGO SEPERTI MAMANG IBRAHIMOVIC")
        self.assertEqual(str(a), "TIPS AND TRICK SUPAYA JAGO SEPERTI MAMANG IBRAHIMOVIC")

    def test_app_name(self):
        self.assertEquals(ArticleConfig.name, 'article')
        self.assertEquals(apps.get_app_config('article').name, 'article')
