from django.db import models
from doctor.models import Doctor
from account.models import Account

class Day(models.Model):
	DAY_OPTION = [
		("Monday", "Monday"),
		("Tuesday", "Tuesday"),
		("Wednesday", "Wednesday"),
		("Thursday", "Thursday"),
		("Friday", "Friday"),
	]
	name = models.CharField(max_length=10, choices=DAY_OPTION)
	doctor = models.ForeignKey(Doctor, on_delete=models.CASCADE)

	def __str__(self):
		return f'{self.doctor} schedule on {self.name}'


class Hour(models.Model):
	HOURS_OPTION = [
		("08.00-09.00", "08.00-09.00"),
		("09.00-10.00", "09.00-10.00"),
		("10.00-11.00", "10.00-11.00"),
		("11.00-12.00", "11.00-12.00"),
		("13.00-14.00", "13.00-14.00"),
		("14.00-15.00", "14.00-15.00"),
		("15.00-16.00", "15.00-16.00"),
		("16.00-17.00", "16.00-17.00"),
	]
	hour = models.CharField(max_length=12, choices=HOURS_OPTION)
	day = models.ForeignKey(Day, on_delete=models.CASCADE)
	available = models.BooleanField(default=True)

	class Meta:
		ordering = ['hour']

	def __str__(self):
		return self.hour

class Booking(models.Model):
	# orderer = models.CharField(max_length=50)
	orderer = models.ForeignKey(Account, on_delete=models.CASCADE)
	doctor = models.ForeignKey(Doctor, on_delete=models.CASCADE)
	time = models.ForeignKey(Hour, on_delete=models.CASCADE)
	email = models.EmailField()
	phone = models.CharField(max_length=15)

	def __str__(self):
		return f'{self.orderer} has booked {self.doctor} on {self.time.day} at {self.time}'
