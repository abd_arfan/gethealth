$(document).ready(function() {

	$("#search").keyup(function() {
		var input = $("#search").val();
		// console.log(input);
		var input1 = input.toLowerCase();
		// panggil dengan teknik AJAX
		$.ajax({
			url: '/schedule/look_up?q=' + input1,
			success : function(output) {
				// console.log(output);

				var days = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday"];

				$(".accordion").empty();

				days.forEach(myFunc);

				function myFunc(hari) {

					// console.log(hari);

					var sotd = new Array();

					for (var i = 0; i < output.length; i++) {
						if (hari.localeCompare(output[i].day.name) == 0) {
							sotd.push(output[i]);
						}
					}

					// console.log(sotd);

					$(".accordion").append(
						"<h4>" + hari + "</h4>" + 
						"<div class=\"my-3\" id=\"" + hari + "\">"
					);

					if (sotd.length != 0) {
						// console.log("sotd.length = " + sotd.length);
						var f = 0;
						for (var i = 0; i < sotd.length; i++) {
							doctor = sotd[i].day.doctor;
							hour = sotd[i].hour;
							hour_id = sotd[i].id;

							// console.log("> " + (i + 1));
							$("#" + hari).append(
								"<div class=\"card border-right-0 border-left-0 border-bottom-0\">" + 
								"<div class=\"card-header\" id=\"heading_" + hour_id + "\">" +
							   	"<a class=\"stretched-link time\" data-toggle=\"collapse\" data-parent=\"#booking-option\" href=\"#collapse_" + hour_id + "\">" + 
						   		hour +
							   	"</a>" + 
							  	"<a href=\"/doctor/" + doctor.id + "\" class=\"doctor\">" +
						  		doctor.name +
							   	"</a>" +
								"</div>" +
								"<div id=\"collapse_" + hour_id + "\" class=\"collapse\" aria-labelledby=\"heading_" + hour_id + "\" data-parent=\"#booking-option\">" +
							   	"<div class=\"card-body\">" +
						        	"<a type=\"button\" class=\"btn btn-genoa\" href=\"booking_form/" + hour_id + "\">Book</a>" +
								"</div>" +
								"</div>" +
								"</div>"
								);
						}
					} else {	
						$("#" + hari).append(
							"<div class=\"card not-available\">" +
								"Sorry, there is no schedule available on this day." +
							"</div>"
						);	
					}

					$("#" + hari).append(
						"</div>"
					);
				}	
			}
		})
	});
})