from django.test import TestCase, Client
from django.urls import resolve, reverse
from django.http import HttpRequest, response
from Schedule.views import index, book
from Schedule.models import Day, Hour, Booking
from doctor.models import Doctor
from account.models import Account
from django.contrib.auth import get_user_model
from .apps import ScheduleConfig
from django.apps import apps


class TestViews(TestCase):
	def setUp(self):
		self.client  = Client()
		self.doctor = Doctor.objects.create(
				name 		='test', 
				degree_name	='test', 
				message		='test', 
				known_for	='test',
				path_to		='test', 
				passion		='test', 
				fav_quote 	='test', 
				fav_quote_by='test'
			)
		self.day  = Day.objects.create(name="Monday", doctor=self.doctor)
		self.hour = Hour.objects.create(hour="12.00 - 13.00", day=self.day, available=True)
		self.user = get_user_model().objects.create_user(
				email 	 = 'dummy@dummy.com',
				username = 'dummy',
				password = 'dummydumdum123',
			)
		self.booking = Booking.objects.create(
				orderer = self.user,
				doctor 	= self.doctor,
				time 	= self.hour,
				email 	= self.user.email,
				phone 	= 'sample'
			)
		self.index_url = reverse("Schedule:index")
		self.form_url  = reverse('Schedule:form', args=[self.hour.id])
		self.log_in	   = self.client.post('/login/', data={
				'email' : 'dummy@dummy.com',
				'password' : 'dummydumdum123',
			})
		self.myappointment = reverse('Schedule:appointmentlist')
		
	def test_bookingpage_GET(self):
		response = self.client.get(self.index_url)
		self.assertEquals(response.status_code, 200)
		self.assertTemplateUsed(response, "Schedule/bookingpage.html")
		self.assertContains(response, "BOOK A SCHEDULE")

	def test_bookingpage_berisi_objek_day_yang_sudah_dibuat(self):
		response = self.client.get(self.index_url)
		self.assertContains(response, "Monday")
		self.assertEquals("Monday", self.hour.day.name)

	def test_bookingpage_berisi_objek_hour_yang_sudah_dibuat(self):
		response = self.client.get(self.index_url)
		self.assertContains(response, "12.00 - 13.00")

	def test_bookingform_GET_usai_login(self):
		response = self.client.get(self.form_url)
		self.assertEquals(response.status_code, 200)
		self.assertTemplateUsed(response, "Schedule/bookingform.html")
		self.assertContains(response, "Booking Form")

	def test_bookingform_POST_buat_objek_Booking_usai_login(self):
		response = self.client.post(self.form_url, {
				"phone" : "test"
			})
		self.assertEquals(response.status_code, 302)
		self.assertEquals(Booking.objects.get(id=2).orderer.username, "dummy")
		self.assertEquals(Booking.objects.get(id=2).time.available, False)

	def test_myappointment_GET_usai_login(self):
		response = self.client.get(self.myappointment)
		self.assertEquals(response.status_code, 200)
		self.assertTemplateUsed(response, "Schedule/myappointments.html")
		self.assertContains(response, "My Appointments")

	def test_bookingform_GET_tapi_belum_login(self):
		self.client.get('/logout/')
		response = self.client.get(self.form_url)
		self.assertEquals(response.status_code, 302)

	def test_myappointment_GET_tapi_belum_login(self):
		self.client.get('/logout/')
		response = self.client.get(self.myappointment)
		self.assertEquals(response.status_code, 404)


class TestModels(TestCase):
	def setUp(self):
		self.doctor = Doctor.objects.create(
				name 		='test', 
				degree_name	='test', 
				message		='test', 
				known_for	='test',
				path_to		='test', 
				passion		='test', 
				fav_quote 	='test', 
				fav_quote_by='test'
			)
		self.day = Day.objects.create(name="Monday", doctor=self.doctor)
		self.hour = Hour.objects.create(hour="12.00-13.00", day=self.day, available=True)
		self.user = get_user_model().objects.create_user(
				email 	 = 'dummy@dummy.com',
				username = 'dummy',
				password = 'dummydumdum123',
			)
		self.booking = Booking.objects.create(
				orderer = self.user,
				doctor 	= self.doctor,
				time 	= self.hour,
				email 	= self.user.email,
				phone 	= 'sample'
			)

	def test_dapat_menyimpan_objek_Day(self):
		day = Day.objects.all().count()
		self.assertEquals(day, 1)

	def test_dapat_menyimpan_objek_Hour(self):
		hour = Hour.objects.all().count()
		self.assertEquals(hour, 1)

	def test_dapat_menyimpan_objek_Booking(self):
		booking = Booking.objects.all().count()
		self.assertEquals(booking, 1)

	def test___str___method_works(self):
		self.assertEquals(
			str(self.booking), 
			f'{self.user} has booked {self.doctor} on {self.day} at {self.hour}'
		)
		self.assertEquals(str(self.hour), "12.00-13.00")
		self.assertEquals(str(self.day), f'{self.doctor} schedule on {self.day.name}')


class AppsTest(TestCase):
	def test_apps(self):
		self.assertEqual(ScheduleConfig.name, 'Schedule')
		self.assertEqual(apps.get_app_config('Schedule').name, 'Schedule')