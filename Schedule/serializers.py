from rest_framework import serializers
from .models import Hour, Day
from doctor.models import Doctor

class DoctorSerializer(serializers.ModelSerializer):
	class Meta:
		model = Doctor
		fields = ("name", "id")

class DaySerializer(serializers.ModelSerializer):
	doctor = DoctorSerializer(read_only=True)

	class Meta:
		model = Day
		fields = "__all__"

class HourSerializer(serializers.ModelSerializer):
	day = DaySerializer(read_only=True)

	class Meta:
		model = Hour
		fields = "__all__"