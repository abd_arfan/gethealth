from django import forms
from .models import Booking

class BookingForm(forms.ModelForm):
	class Meta:
		model = Booking
		fields = [
			'phone',
		]
		widgets = {
			'phone' : forms.TextInput(attrs={'class' : 'form-control'})
		}
	error_messages = {
		'required' : 'Please fill this field'
	}
