from django.test import TestCase
from django.test import Client
from django.urls import resolve, reverse
from django.http import HttpRequest, response
from django.contrib.auth import get_user_model # used to get user in django.auth

from account.models import Account
import account.views
from django.conf import settings



from doctor.models import Doctor, Comment
import doctor.views 


#How to test: coverage run --source='doctor' manage.py test doctor
class DetailPageUnit(TestCase):
	def test_url_doctor_ada_url_dengan_HTTP_respon_yang_tepat(self):
		response = Client().get('/doctor/')
		self.assertEqual(response.status_code, 200)
	def test_url_doctor_menggunakan_HTML_yang_telah_ditentukan(self):
		response = Client().get('/doctor/')
		self.assertTemplateUsed(response,"doctor/listDoctor.html")
	def test_model_doctor_dapat_menyimpan_model(self):
		new_doctor = Doctor.objects.create(name='test', degree_name='test', message='test', known_for='test',
					 path_to='test', passion='test', fav_quote='test', fav_quote_by='test')
		count = Doctor.objects.all().count()
		self.assertEqual(count,1)
	def test_apakah_dalam_file_html_ada_kata_atau_kalimat_yang_diwajibkan_untuk_ada(self):
		response = Client().get('/doctor/')
		html_kembalian = response.content.decode('utf8')
		self.assertIn('Meet Our Doctors', html_kembalian)
		self.assertIn('Book Us!', html_kembalian)
	def test_url_detail_doctor_memiliki_HTTP_respon_dan_menggunakan_html_yang_tepat(self):
		#dibuat objek dokter agar dapat melihat detail dokter
		new_doctor = Doctor.objects.create(name='test', degree_name='test', message='test', known_for='test',
					 path_to='test', passion='test', fav_quote='test', fav_quote_by='test')
		response = Client().get('/doctor/1')
		self.assertEqual(response.status_code, 200)
		self.assertTemplateUsed(response, "doctor/detailDoctor.html")
	
	# TES MODUL DAN VIEW UNTUK COMMENT/ULAS DOKTER
	
	def test_dapat_membuat_model_comment(self):
		new_doctor = Doctor.objects.create(name='test', degree_name='test', message='test', known_for='test',
					 path_to='test', passion='test', fav_quote='test', fav_quote_by='test',)
		new_comment = Comment.objects.create(username="user", text="test", doctor=new_doctor)
		count = Comment.objects.all().count()
		self.assertEqual(count, 1)
	
	def test_views_detail_tidak_dapat_mempost_saat_user_tidak_authenticated(self):
		#dibuat objek dokter agar dapat melihat detail dokter
		new_doctor = Doctor.objects.create(name='test', degree_name='test', message='test', known_for='test',
					 path_to='test', passion='test', fav_quote='test', fav_quote_by='test')
		#test
		response = self.client.post('/doctor/addcomment', data={
            "doctorID" : 1, "comment" : "test"})
		self.assertEqual(response.status_code, 302)
		count = Comment.objects.all().count()
		self.assertEqual(count, 0)
	
	def test_views_detail_dapat_mempost_saat_user_authenticated(self):
		#dibuat objek dokter agar dapat melihat detail dokter
		new_doctor = Doctor.objects.create(name='test', degree_name='test', message='test', known_for='test',
					 path_to='test', passion='test', fav_quote='test', fav_quote_by='test')
		# register and login user
		email = 'test@gmail.com'
		username = 'uname'
		password1 = 'te3S@tPass'
		response = self.client.post(reverse('register'), data={
			"email" : email,
			"username" : username,
			"password1" : password1,
			"password2" : password1,})
		response = self.client.post('/login/', data={
            "email" : email, "password": password1})
		#test post
		response = self.client.post('/doctor/addcomment', data={
            "doctorID" : 1, "comment" : "test"})
		self.assertEqual(response.status_code, 302)
		count = Comment.objects.all().count()
		self.assertEqual(count, 1)
	
	def test_fungsi_MyJsonFunction_mengembalikan_json_yang_dapat_dibaca_melalui_views(self):
		# create doctor
		new_doctor = Doctor.objects.create(name='test', degree_name='test', message='test', known_for='test',
					 path_to='test', passion='test', fav_quote='test', fav_quote_by='test')
		# create comment
		new_comment = Comment.objects.create(username="user", text="test", doctor=new_doctor)
		
		response = Client().get('/doctor/commentList/1') # 1 as the doctor's id
		self.assertEqual(response.status_code, 200)
		
'''class testListDoctor(TestCase):
	def test_like_button(self):'''
		