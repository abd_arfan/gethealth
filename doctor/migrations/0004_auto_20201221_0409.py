# Generated by Django 3.1.4 on 2020-12-20 21:09

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('doctor', '0003_comment_text'),
    ]

    operations = [
        migrations.AlterField(
            model_name='doctor',
            name='profile_picture',
            field=models.ImageField(default='default.jpg', upload_to='Profile_Picture/'),
        ),
    ]
