from django.urls import path
from . import views

app_name = 'doctor'

urlpatterns = [
	path('', views.ListDoctor, name="listdoctor"),
	path('<int:id>', views.DetailDoctor),
	path('addcomment', views.AddComment, name="addComment"),
	path('commentList/<int:id>', views.MyJsonFunction, name="getCommentList"),
	path('like/<int:id>', views.LikeButton, name="LikeButton"),
]
