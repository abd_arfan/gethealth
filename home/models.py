from django.db import models

class Suggestions(models.Model):
	name = models.CharField(blank=False, max_length=100)
	subject = models.CharField(blank=False, null=False, default="No subject", max_length=70)
	suggestion = models.TextField(blank=False)

	def __str__(self):
		return self.name