from django import forms
from .models import Suggestions

class SuggestionForm(forms.Form):

	name_attrs = {
		'class' : 'form-control',
		'type' : 'text',
		'required' : True,
		'placeholder' : 'Name'
	}

	subject_attrs = {
		'class' : 'form-control',
		'type' : 'text',
		'required' : True,
		'placeholder' : 'Subject'
	}

	suggestion_attrs = {
		'class' : 'form-control',
		'type' : 'text',
		'required' : True,
		'placeholder' : 'Write your suggestion here..'
	}


	name = forms.CharField(label="", max_length=100, widget=forms.TextInput(attrs=name_attrs))
	subject = forms.CharField(label="", max_length=70, widget=forms.TextInput(attrs=subject_attrs))
	suggestion = forms.CharField(label="", widget=forms.Textarea(attrs=suggestion_attrs))
