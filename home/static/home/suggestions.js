$(document).ready(function() {
	$.ajax({
		url: '/process_suggestions',
		success: function(data) {
			console.log(data.length)
			if(data.length > 0) {
				$(".list_of_suggestions").append("Suggestions")
				$(".table").append(
					"<thead><tr><th scope='col'>No</th><th scope='col'>Name</th>\
					<th scope='col'>Subject</th><th scope='col'>Suggestion</th></tr></thead>");
				for (i=0; i<data.length; i++) {
					var name = data[i].fields.name;
					var subject = data[i].fields.subject;
					var suggestion = data[i].fields.suggestion;
					console.log(name)
					$(".table").append(
						"<tr class='tr'> <th scope='row'>" + (i+1) + "</th> <td>" + name + "\
						</td><td>" + subject + "</td><td>" + suggestion +"</td></tr>");
				}
			} else {
				$(".list_of_suggestions").append("There is no suggestion.")
				$(".list_of_suggestions").css("padding-top", "200px")
				$(".list_of_suggestions").css("padding-bottom", "160px")
			}
		}
	})
})