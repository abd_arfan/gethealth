# gethealth

Tugas Kelompok 2 untuk PPW kelas D kelompok D12

Anggota kelompok:
- Abdurrahman Arfan
- Alif Iqbal Hazairin
- Dwi Ayu Sekarini Pandjaitan
- Naufal Sani
- Zalfa Luqyana Akifah

DESKRIPSI TUGAS:
Arfan: searchbox artikel, Comment artikel (harus login)

Iqbal: Filter jadwal (ajax), Pesan jadwal (harus login)

Sani: Taroh testimoni di detail dokter (harus login), list testimoni tiap 5 testimoni swipe left/right (ajax)

Rini: Search box list doctor (ajax), tombol "REKOMENDASI" (login)

Zalfa: Suggestion untuk perkembangan website (harus login), mengirim data suggestion secara asynchronous (ajax) dan menampilkan kasus covid-19 hari ini di Indonesia pada index.html menggunakan api https://api.kawalcorona.com/ (ajax)

Situs web : https://gethealth.herokuapp.com/

#INFO STATUS:
[![pipeline status](https://gitlab.com/abd_arfan/gethealth/badges/master/pipeline.svg)](https://gitlab.com/abd_arfan/gethealth/-/commits/master)
[![coverage report](https://gitlab.com/abd_arfan/gethealth/badges/master/coverage.svg)](https://gitlab.com/abd_arfan/gethealth/-/commits/master)